import * as React from "react";
import useContentApi from "../hooks/ContentApiContext";

export interface IContentListItemComponentProps {
    name: string;
    latest: boolean;
}

export function ContentListItemComponent(props: IContentListItemComponentProps) {
    const contentApi = useContentApi();

    return <>
        <a href={contentApi.getByNameUrl(props.name)}>{props.name}{props.latest ? " (latest)" : ""}</a>
    </>
}

export default ContentListItemComponent;
