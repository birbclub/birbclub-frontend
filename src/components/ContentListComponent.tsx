import * as React from "react";
import ContentListItemComponent from "./ContentListItemComponent";

export interface IContentListComponentProps {
    items: string[];
}

export function ContentListComponent(props: IContentListComponentProps) {
    const items = props.items
        .sort()
        .reverse()
        .map((name, idx) =>
            <li key={name}>
                <ContentListItemComponent name={name} latest={idx == 0}/>
            </li>
        );

    return <>
        <ul>{items}</ul>
    </>
}

export default ContentListComponent;
