import * as React from "react";

export interface IServerStatusComponentProps {
    version: string;
}

export function ServerStatusComponent(props: IServerStatusComponentProps) {
    return <>
        <p>Server URL: birbclub.siberianbot.me:25565</p>
        <p>Server version: {props.version}</p>
    </>
}

export default ServerStatusComponent;
