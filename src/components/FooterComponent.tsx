import * as React from "react";

export function FooterComponent() {
    return <>
        <footer className="flex justify-content-center">
            <p>Made by <a target="_blank" href="https://siberianbot.me">siberianbot</a>. Checkout our <a target="_blank" href="https://gitlab.com/birbclub">GitLab</a> project.</p>
        </footer>
    </>
}

export default FooterComponent;
