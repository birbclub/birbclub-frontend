import * as React from "react";

export function InstructionComponent() {
    return <>
        <h2>Instructions</h2>
        <ol>
            <li>Download and install FabricMC from <a target="_blank" href="https://fabricmc.net/use/installer/">here</a>;</li>
            <li>Download mod-pack and extract into /mods directory.</li>
        </ol>
    </>
}

export default InstructionComponent;
