import * as React from "react";
// @ts-ignore
import logo from "../images/logo.jpg";

export function HeaderComponent() {
    return <>
        <header className="flex justify-content-center align-items-center">
            <div className="logo-container">
                <img className="logo" alt="Birb Club logo" src={logo}/>
            </div>
            <h1>Birb Club</h1>
        </header>
    </>
}

export default HeaderComponent;
