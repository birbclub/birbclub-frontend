import * as React from "react";

export interface IPlayerComponentProps {
    name: string;
}

export function PlayerComponent(props: IPlayerComponentProps) {
    return <>{props.name}</>
}

export default PlayerComponent;
