import * as React from "react";
import ContentListComponent from "./ContentListComponent";
import useContentApi from "../hooks/ContentApiContext";
import ApiError from "../api/api-error";

export function ContentComponent() {
    const [state, setState] = React.useState<string[]>(null);
    const [error, setError] = React.useState<string>(null);
    const contentApi = useContentApi();

    React.useEffect(() => {
        contentApi
            .getAll()
            .then(
                response => {
                    setError(null);
                    setState(response.content);
                },
                error => error instanceof ApiError
                    ? setError(error.message)
                    : setError("Unable to load content due to unknown error 😐"))
    }, []);

    if (error != null) {
        return <>
            <h2>{error}</h2>
        </>
    }

    if (state == null) {
        return <>
            <h2>List of mod-packs is not ready. 😐</h2>
        </>
    }

    return <>
        <h2>Mod-packs</h2>
        <ContentListComponent items={state}/>
    </>
}

export default ContentComponent;
