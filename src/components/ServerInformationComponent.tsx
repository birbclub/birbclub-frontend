import * as React from "react";
import PlayersComponent from "./PlayersComponent";
import ServerStatusComponent from "./ServerStatusComponent";
import {useApi} from "../hooks/ApiContext";
import ApiError from "../api/api-error";
import {IStatResponse} from "../api/api-v1";

export function ServerInformationComponent() {
    const [state, setState] = React.useState<IStatResponse>(null);
    const [error, setError] = React.useState<string>(null);
    const api = useApi();

    const queryFn = () => {
        api.getServerStat()
            .then(
                response => {
                    setState(response);
                    setError(null);
                },
                error => error instanceof ApiError
                    ? setError(error.message)
                    : setError("Unable to get server information due to unknown error 😐"))
            .then(() => setTimeout(queryFn, 3000))
    };

    React.useEffect(() => queryFn(), []);

    if (error != null) {
        return <>
            <h2>{error}</h2>
        </>
    }

    if (state == null) {
        return <>
            <h2>Server information is not ready. 😐</h2>
        </>
    }

    return <>
        <h2>Server Information</h2>
        <ServerStatusComponent version={state.version}/>
        <PlayersComponent players={state.players}/>
    </>
}

export default ServerInformationComponent;
