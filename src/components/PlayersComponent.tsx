import * as React from "react";
import PlayerComponent from "./PlayerComponent";

export interface IPlayersComponentProps {
    players: string[];
}

export function PlayersComponent(props: IPlayersComponentProps) {
    const playersCount = props.players.length;

    if (playersCount == 0) {
        return <>
            <h3>There is no players at all 😐</h3>
        </>
    }

    const items = props.players
        .map((name) =>
            <li key={name}>
                <PlayerComponent name={name}/>
            </li>
        )

    return <>
        <h3>There is {playersCount} {playersCount == 1 ? "player" : "players"}:</h3>
        <ul>{items}</ul>
    </>
}

export default PlayersComponent;
