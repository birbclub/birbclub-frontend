import * as React from "react";

export function MainItemLayout({children}) {
    return <>
        <div className="main-item">
            {children}
        </div>
    </>
}

export default MainItemLayout;
