import * as React from "react";

export function MainLayout({children}) {
    return <>
        <main className="flex flex-wrap justify-content-center">
            {children}
        </main>
    </>
}

export default MainLayout;
