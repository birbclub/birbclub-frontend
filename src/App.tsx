import * as React from "react";

import HeaderComponent from "./components/HeaderComponent";
import InstructionComponent from "./components/InstructionComponent";
import ContentComponent from "./components/ContentComponent";
import ServerInformationComponent from "./components/ServerInformationComponent";
import FooterComponent from "./components/FooterComponent";
import MainLayout from "./layouts/MainLayout";
import MainItemLayout from "./layouts/MainItemLayout";
import {ApiContext} from "./hooks/ApiContext";
import {ContentApiContext} from "./hooks/ContentApiContext";
import ContentApiV1 from "./api/content-api-v1";
import ApiV1 from "./api/api-v1";

const api = new ApiV1(process.env["BIRBCLUB_API"]);
const contentApi = new ContentApiV1(process.env["BIRBCLUB_CONTENT_API"]);

export function App() {
    return <>
        <ApiContext.Provider value={api}>
            <ContentApiContext.Provider value={contentApi}>
                <HeaderComponent/>
                <MainLayout>
                    <MainItemLayout>
                        <ServerInformationComponent/>
                    </MainItemLayout>
                    <MainItemLayout>
                        <InstructionComponent/>
                    </MainItemLayout>
                    <MainItemLayout>
                        <ContentComponent/>
                    </MainItemLayout>
                </MainLayout>
                <div className="flex-gutter"/>
                <FooterComponent/>
            </ContentApiContext.Provider>
        </ApiContext.Provider>
    </>
}

export default App;
