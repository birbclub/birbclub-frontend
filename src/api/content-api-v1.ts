import ApiError from "./api-error";

export interface IGetAllResponse {
    content: string[];
}

export class ContentApiV1 {
    constructor(private readonly _baseUrl: string) {
        //
    }

    public async getAll(): Promise<IGetAllResponse> {
        const url = `${this._baseUrl}/v1/content`;
        const response = await fetch(url, {method: "GET"});

        if (response.status != 200) {
            throw new ApiError("There is a problem with loading all available mod-packs 😐");
        }

        return await response.json() as IGetAllResponse;
    }

    public getByNameUrl(name: string): string {
        return `${this._baseUrl}/v1/content/${name}`;
    }
}

export default ContentApiV1;
