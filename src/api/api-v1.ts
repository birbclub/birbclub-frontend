import ApiError from "./api-error";

export interface IStatResponse {
    version: string,
    players: string[]
}

export class ApiV1 {
    constructor(private readonly _baseUrl: string) {
        //
    }

    public async getServerStat(): Promise<IStatResponse> {
        const url = `${this._baseUrl}/v1/stat`
        const response = await fetch(url, {method: "GET"});

        if (response.status != 200) {
            throw new ApiError("There is a problem with retrieving statistics from our server 😐");
        }

        return await response.json() as IStatResponse;
    }
}

export default ApiV1;
