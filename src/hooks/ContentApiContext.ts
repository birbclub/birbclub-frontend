import * as React from "react";
import ContentApiV1 from "../api/content-api-v1";

export const ContentApiContext = React.createContext<ContentApiV1>(null);

export function useContentApi() {
    return React.useContext(ContentApiContext);
}

export default useContentApi;
