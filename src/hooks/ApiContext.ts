import * as React from "react";
import ApiV1 from "../api/api-v1";

export const ApiContext = React.createContext<ApiV1>(null);

export function useApi() {
    return React.useContext(ApiContext);
}

export default useApi;
