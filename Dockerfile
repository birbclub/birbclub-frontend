FROM node AS build-env

ARG BIRBCLUB_API="/api"
ARG BIRBCLUB_CONTENT_API="/content"

ENV BIRBCLUB_API=$BIRBCLUB_API
ENV BIRBCLUB_CONTENT_API=$BIRBCLUB_CONTENT_API

WORKDIR /app

COPY ./package.json /app/
COPY ./yarn.lock /app/
COPY ./.env /app/
COPY ./src/ /app/src/

RUN yarn
RUN yarn build

FROM nginx

COPY --from=build-env /app/dist/ /usr/share/nginx/html/

EXPOSE 80

ENTRYPOINT ["nginx","-g","daemon off;"]
